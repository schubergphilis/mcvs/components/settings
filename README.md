MCVS-settings-guard - the solution for effortlessly managing and enforcing 
repository settings on GitLab! In the fast-paced world of software development, 
ensuring that repositories adhere to security standards and compliance 
measures is crucial. MCVS-settings-guard simplifies this process, allowing to 
automatically adjust and enforce repository settings, providing with a robust 
and audit-ready environment.

This repository contains a component that can be included in a pipeline of a
project to ensure that GitLab settings can be adjusted in an entire group to
ensure that it is compliant. Note: the equivalent for GitHub can be found
[here](https://github.com/schubergphilis/mcvs-settings-action).

The following Gitlab settings can be adjusted:
##### Merge requests
* Merge requests enabling 
* Squash option
* Merge request method
* "Delete source branch" option
* Shows links to create or view a merge request when pushing from the command line option
* Pipelines must succeed before merging option
* All threads must be resolved before merging option
##### Merge requests approvals
* Adjust the number of approvals required
* Prevent approval by author option
* Prevent approvals by users who add commits option
* Prevent editing approval rules in merge requests option
* Remove all approvals when a commit is added option
##### Protected branches
* Adjust who are allowed to merge on default branch
* Adjust who are allowed to push and merge on default branch
* Adjust who are allowed to force push on default branch
* Code owner approval on default branch option

# Usage 

## Step 1: set up a mirror

Mirror this repository as GitLab does not allow to use CI/CD catalog components
outside a GitLab landingzone and add a
`MCVS_COMPONENTS_SETTINGS_RELEASE_JOB_RUNNER_TAG` CI/CD variable if required.

Settings > Repository > Mirroring repositories:
- Git repository URL:
`https://gitlab.com/schubergphilis/mcvs/components/settings.git`
- Mirror direction: pull
- Overwrite diverged branches: true

#### Change the default .gitlab-ci.yml

Settings > CI/CD > General pipelines > CI/CD configuration file:
`.gitlab-ci-release.yml`

#### Add a description to the mirrored repository

Settings > General > Project description (optional)
`The settings component guards the settings of a GitLab repository.`

#### Enable CI/CD Catalog

Settings > Visibility project features, permissions, set
`CI/CD Catalog resource` to true

## Step 2: Create a `.gitlab-ci.yml`
for example:

```bash
---
include:
  - component: ${GITLAB_URL}/${PROJECT_PATH}/component@0.1.0
    inputs:
      gitlab_project_id: 123456789
      branch_default_name: main
      settings_squash: always
      settings_merge_method: merge
      settings_merge_requests: true
      settings_remove_source_branch_after_merge: true
      settings_printing_merge_request_link_enabled: true
      settings_only_allow_merge_if_pipeline_succeeds: true
      settings_only_allow_merge_if_all_discussions_are_resolved: true
      protected_branches_allowed_to_merge: 40
      protected_branches_allowed_to_push: 0
      approval_rule_name: set_by_settings_guard
      number_of_approvals: 1
      approval_protected_branches: true
      group_id_approval: 123456789
      approval_settings_merge_requests_disable_committers_approval: true
      approval_settings_merge_requests_author_approval: false
      approval_settings_disable_overriding_approvers_per_merge_request: true
      approval_settings_reset_approvals_on_push: true
      tags: some-role
      token: ${API_TOKEN}
```
This `.gitlab-ci.yml` includes the MCVS-settings-guard component 
and the input parameters that allow you to determine the prefered settings.
| Inputs | Description | Type | Default | Options | 
| --- | --- | --- | --- | --- |
| gitlab_project_id | Project IDs of the relevant projects whose settings are to be guarded | number | N/A. | N/A. |
| branch_default_name | The name of the default branch | number | main | main, master |
| squash | Set the option for squash commits when merging | string | always | always, default_on |
| settings_merge_method | Set the merge method | string | merge | merge, rebase_merge |
| settings_merge_requests | Set the merge request option | boolean | true | true, false |
| settings_remove_source_branch_after_merge | Enables "Delete source branch" after merge option | boolean | true | true, false |
| settings_printing_merge_request_link_enabled | Shows link to create or view a merge request when pushing form the command line | boolean | true | true, false |
| settings_only_allow_merge_if_pipeline_succeeds | Set merge requests can't be merged if the latest pipeline did not succeed or is still running option | boolean | true | true, false |
| settings_only_allow_merge_if_all_discussions_are_resolved | Set all threads must be resolved before merging option | boolean | true | true, false |
| protected_branches_allowed_to_merge | Who is allowed to merge on protected branches | number | 40 | 0 => No access, 30 => Developer access, 40 => Maintainer access, 60 => Admin access |
| protected_branches_allowed_to_push | Who is allowed to push and merge on protected branches | number | 0 | 0 => No access, 30 => Developer access, 40 => Maintainer access, 60 => Admin access |
| approval_rule_name | Name of the approval rule | string | set_by_settings_guard | N/A. |
| number_of_approvals | Set the amount of approvals before merging | number | 1 | 0:100 |
| approval_protected_branches | Whether the approval rule applies to all protected branches | boolean | true | true, false |
| group_id_approval | 	The IDs of group(s) as approvers on a merge request | number | N/A. | N/A. |
| approval_settings_merge_requests_disable_committers_approval | Allow or prevent committers from self approving merge requests | boolean | true | true, false |
| approval_settings_merge_requests_author_approval | Allow or prevent authors from self approving merge requests; true means authors can self approve | boolean | false | true, false |
| approval_settings_disable_overriding_approvers_per_merge_request| Allow or prevent overriding approvers per merge request | boolean |true | true, false |
| approval_settings_reset_approvals_on_push | Reset approvals on a new push | boolean | true | true, false | true
| tags | Tag being used for the runner | string | [] | N/A. |
| token | The API token | CI/CD variable | ${CI_JOB_TOKEN} | N/A. |